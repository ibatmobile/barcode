import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Rx';
//import { ??? } from '../model/???.model';

@Injectable()
export class ProcessBarCodeService {
  API_ENDPOINT: string;
  API_KEY: string;
  API_URL: string;





  constructor(public http: Http) {
    this.API_ENDPOINT = "http://peopleserver.localhost/ecf/api/profile";
    this.API_KEY = "";
    this.API_URL = this.API_ENDPOINT + this.API_KEY

  }

  //https://scotch.io/tutorials/angular-2-http-requests-with-observables
  getListing(): Observable<any[]> {
    let serviceURL = this.API_URL;
    
    return this.http.get(serviceURL)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw('Server error')

      );
  }


}
