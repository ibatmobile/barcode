import { Component } from '@angular/core';
import { Platform, Alert } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { WebApiObservableService } from '../../model/web-api-observable.service';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    errorMessage: any;
    constructor(private printingObservableService: WebApiObservableService,

        public navCtrl: NavController, public platform: Platform, private barcodeScanner: BarcodeScanner) {

        //this.

    }

    ScanCertQR1() {

        let certIDs = "38598|38599|38600|38601|38602|38603|38604|38605|38606|38607";

        let listOfCertsPrinted = { certificateIDs: certIDs.split('|') };
        //            .createService('http://localhost:50981/api/certificates/markPrintedCerts', listOfCertsPrinted)
        this.printingObservableService

            .createService('https://cms.ics-skills.net/', listOfCertsPrinted)
            .subscribe(
            result => console.log("Processed"),
            error => this.errorMessage = <any>error
            );

    }

    ScanCertQR() {

        this.barcodeScanner.scan().then((barcodeData) => {
            // Success! Barcode data is here
            alert(barcodeData.text);

            let listOfCertsPrinted = { certificateIDs: barcodeData.text.split('|') };

            this.printingObservableService

                .createService('https://cms.ics-skills.net/api/certificates/markPrintedCerts', listOfCertsPrinted)
                .subscribe(
                result => console.log("Processed"),
                error => this.errorMessage = <any>error
                );



        }, (err) => {
            // An error occurred
            alert('error')
        });

        /*
             this.platform.ready().then(() => {
                    cordova.plugins.barcodeScanner.scan((result) => {
                        this.navCtrl.present(Alert.create({
                            title: "Scan Results",
                            subTitle: result.text,
                            buttons: ["Close"]
                        }));
                    }, (error) => {
                        this.navCtrl.present(Alert.create({
                            title: "Attention!",
                            subTitle: error,
                            buttons: ["Close"]
                        }));
                    });
                });
                */

    }

}
